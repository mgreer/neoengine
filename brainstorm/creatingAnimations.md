# Creating animationDef

const entityAnimations = new EntityAnimations();

if (animationDef is object) {
    for (let y = 0; y < img.height; y += animationDef.height * 16) {
        const animation = new Animation();

        for (let x = 0; x < img.width; x += animationDef.width * 16) {
            const frame = extractFrame(x, y, animationDef.width, animationDef.height);

            if (!isEmpty(frame)) {
                animation.addFrame(frame);
            }
        }

        entityAnimations.addAnimation(animation);
    }
} else {
    // handle when animations are of different size in the same image
}

const { tiles, animationSrc } = entityAnimations.calculate();

