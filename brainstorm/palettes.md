# Palettes

Need to extract and form the palettes from the input pngs

## overall approach

1. extract all the tiles out into an array
2. each tile will have their own palette
    * sort colors in the palettes, float TRANSPARENT_COLOR to the top
    * group the palettes by compatibility
    * number the master compatibility palettes from 1 to n
3. give each tile its corresponding compatibility palette
4. have tiles index themselves against the palette

Then from there write out the data as before, but also write out the palettes

Before writing out the palettes, convert them from 24bits to 15bits
