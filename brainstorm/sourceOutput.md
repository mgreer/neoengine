# Source Output

Using Sonic as an example

sonic\_entity.h
```

struct Tile tiles_sonic_walk_0_0[] = {
    { .index = 0, .palette = PALETTE_SONIC_0, .mirror = 0, .scale = 0xFFF },
    { .index = 1, .palette = PALETTE_SONIC_0, .mirror = 0, .scale = 0xFFF },
    { .index = 2, .palette = PALETTE_SONIC_1, .mirror = 0, .scale = 0xFFF }
};

struct Sprite sprite_sonic_walk_0_0 = {
    .tiles = tiles_sonic_walk_0_0,
    .sticky = 0
};

struct Sprite* sprites_sonic_walk_0[] = {
    &sprite_sonic_walk_0_0,
    &sprite_sonic_walk_0_1,
    &sprite_sonic_walk_0_2
}

struct Frame frame_sonic_walk_0 = {
    .sprites = sprites_sonic_walk_0,
    .duration = 4
};

struct Frame* frames_sonic_walk[FRAMES_SONIC_WALK_COUNT] = {
    &frame_sonic_walk_0
};

struct Animation animation_sonic_walk = {
    .frames = frames_sonic_walk,
};

struct Animation* animations_sonic[] = {
    &animation_sonic_walk
};

struct Entity sonic_entity = {
    .type = ENTITY_SONIC,
    .animations = animations_sonic
    .currentAnimation = ANIMATION_SONIC_WALK
};



