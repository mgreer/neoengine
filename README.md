# Neo Sprite Tool

A tool for converting pngs into sprites for Neo Geo games

## Status

Very early. It generates all the tiles needed for sprites, but does not account for many things yet

## To install

`npm install -g @city41/neospritetool`

## To use

Define a JSON file explaining your sprites. Check out [NeoBrick's file](https://github.com/city41/neobricks/blob/master/resources/sprites.json) for a reference

Then run the tool: `neosprite tool -i <path to the json file> -h <output directory for the header files> -c <output directory for the CROM binary files>`

The header file will be written in C and look something like this

```
#pragma once

// brick tile indices
#define BRICK_TILE_0 0

// paddle tile indices
#define PADDLE_TILE_0 1
```

The CROM binary files, output.c1 and output.c2, will match the header. In this case, the very first tile in the CROM files will be the brick tile, followed by the paddle tile

## Goals

### Animating Sprites
Using a format very similiar to [SGDK's rescomp](https://github.com/Stephane-D/SGDK/blob/master/bin/rescomp.txt), extract sprite tiles out of the pngs, and generate C header files to define which tiles make up which animation. Tiles will be deduped.

### Auto Animation
Automatically put auto tiles in the right spots in CROM so that you can auto animate them

### Background Layers
Again using a format similar to rescomp, take a background image and generate tiles for it, then generate a tile map for it to load the background into a game. Tiles will be deduped and the sticky bit will be utilized.

### Stretch Goal: Incorporate tiles from other games
Extract out tiles from existing games and then re-ingest them into your game. Great for quickly getting graphics to play with
