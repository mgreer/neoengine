import fs from "fs";
import { Canvas, Image } from "canvas";
import { CROMTile } from "../cromTile";
import { Raw24BitPalette, PaletteColorData } from "../palettes/interfaces";

export type EyecatcherResult = {
    tileCount: number;
    cOddData: number[];
    cEvenData: number[];
};

const TILE_WIDTH = 16;
const TILE_HEIGHT = TILE_WIDTH;
const CORNER_WIDTH = TILE_WIDTH / 2;
const CORNER_HEIGHT = TILE_HEIGHT / 2;
const EYECATCHER_WIDTH_IN_TILES = 15;
const EYECATCHER_HEIGHT_IN_TILES = 4;
const EYECATCHER_WIDTH_IN_PX = EYECATCHER_WIDTH_IN_TILES * TILE_WIDTH;
const EYECATCHER_HEIGHT_IN_PX = EYECATCHER_HEIGHT_IN_TILES * TILE_HEIGHT;

class Eyecatcher {
    constructor(private path: string) {}

    private _tiles: CROMTile[] | null = null;

    private getContext() {
        const fileData = fs.readFileSync(this.path);

        const img = new Image();
        // @ts-ignore still figuring out the node-canvas types
        img.src = fileData;

        const canvas = new Canvas();
        canvas.width = img.width;
        canvas.height = img.height;

        const context = canvas.getContext("2d") as CanvasRenderingContext2D;
        context.drawImage(img, 0, 0);

        return context;
    }

    private separateIntoCorners(
        imgContext: CanvasRenderingContext2D,
        x: number,
        y: number
    ): Uint8ClampedArray[] {
        return [
            // upper right comes first
            imgContext.getImageData(x + 8, y + 0, CORNER_WIDTH, CORNER_HEIGHT)
                .data,
            // then lower right
            imgContext.getImageData(x + 8, y + 8, CORNER_WIDTH, CORNER_HEIGHT)
                .data,
            // then upper left
            imgContext.getImageData(x, y, CORNER_WIDTH, CORNER_HEIGHT).data,
            // then lower left
            imgContext.getImageData(x, y + 8, CORNER_WIDTH, CORNER_HEIGHT).data
        ];
    }

    getTiles(): CROMTile[] {
        if (!this._tiles) {
            const context = this.getContext();
            this._tiles = [];

            for (let cy = 0; cy < EYECATCHER_HEIGHT_IN_PX; cy += 16) {
                for (let cx = 0; cx < EYECATCHER_WIDTH_IN_PX; cx += 16) {
                    const index = (cy / 16) * EYECATCHER_WIDTH_IN_PX + cx / 16;

                    // skip the two corners on the right/top and right/bottom that
                    // don't get included
                    if (index == 14 || index == 59) {
                        continue;
                    }

                    const corners = this.separateIntoCorners(context, cx, cy);
                    this._tiles.push(new CROMTile(corners));
                }
            }
        }
        return this._tiles;
    }

    buildData(
        indexedPaletteDataMap: Map<Raw24BitPalette, PaletteColorData>,
        tileOffset: number
    ): EyecatcherResult {
        if (!this._tiles) {
            throw new Error("Eyecatcher#buildData: called before getTiles");
        }

        this._tiles.forEach(tile => {
            const indexedPaletteData = indexedPaletteDataMap.get(
                tile.rgbPalette
            );

            if (!indexedPaletteData) {
                throw new Error(
                    "EntityAnimations#buildData: failed to get indexedPaletteData for a tile"
                );
            }

            tile.index(indexedPaletteData);
            tile.createCData();
        });

        const cOddData = this._tiles.reduce(
            (buildingCOddData, tile) => {
                return buildingCOddData.concat(tile.cOddData!);
            },
            [] as number[]
        );

        const cEvenData = this._tiles.reduce(
            (buildingCEvenData, tile) => {
                return buildingCEvenData.concat(tile.cEvenData!);
            },
            [] as number[]
        );

        return {
            tileCount: this._tiles.length,
            cOddData,
            cEvenData
        };
    }
}

export { Eyecatcher };
