import { BackgroundDef } from "./interfaces";
import { Background } from "./background";
import { CROMTile } from "../cromTile";
import { Raw24BitPalette, PaletteColorData } from "../palettes/interfaces";
import { TileBinaryData } from "../entities/interfaces";

export type BackgroundsResult = {
    tileCount: number;
    cData: TileBinaryData;
    headerFileData: string;
    sourceFileData: string;
};

class Backgrounds {
    constructor(backgroundDefs: BackgroundDef[], private inputJsonDir: string) {
        this.backgrounds = backgroundDefs.map(bd => new Background(bd));
    }

    _tiles: CROMTile[] | null = null;
    backgrounds: Background[];

    private buildFinalCData(tileDatas: TileBinaryData[]): TileBinaryData {
        return tileDatas.reduce(
            (b, td) => {
                b.cOddData = b.cOddData.concat(td.cOddData);
                b.cEvenData = b.cEvenData.concat(td.cEvenData);

                return b;
            },
            { cOddData: [], cEvenData: [] }
        );
    }

    private buildFinalHeaderFileData(headerDatas: string[]): string {
        return `#pragma once
#include <ng/neogeo.h>
#include "background.h"

${headerDatas.join("\n")}`;
    }

    private buildFinalSourceFileData(sourceDatas: string[]): string {
        return `#include "backgroundDefs.h"

${sourceDatas.join("\n")}`;
    }

    getTiles() {
        if (!this._tiles) {
            this._tiles = this.backgrounds.reduce(
                (buildingTiles, bg) => {
                    return buildingTiles.concat(bg.getTiles(this.inputJsonDir));
                },
                [] as CROMTile[]
            );
        }

        this.backgrounds.forEach(b => b.dumpThenFixBadTiles(this.inputJsonDir));

        return this._tiles;
    }

    buildData(
        indexedPaletteDataMap: Map<Raw24BitPalette, PaletteColorData>,
        tileOffset: number
    ): BackgroundsResult {
        if (!this._tiles) {
            throw new Error("Backgrounds#buildData: called before getTiles");
        }

        const tileMap: Map<CROMTile, number> = new Map<CROMTile, number>();
        this._tiles.forEach((tile, i) => tileMap.set(tile, i + tileOffset));

        const backgroundResults = this.backgrounds.map((bg, i) => {
            const backgroundResult = bg.buildData(
                indexedPaletteDataMap,
                tileMap,
                i
            );

            return backgroundResult;
        });

        const headerData = backgroundResults.map(fr => fr.headerData);
        const sourceData = backgroundResults.map(fr => fr.sourceData);

        const cData = this.buildFinalCData(
            backgroundResults.map(bgr => bgr.cData)
        );
        return {
            tileCount: cData.cOddData.length / 64,
            cData,
            headerFileData: this.buildFinalHeaderFileData(headerData),
            sourceFileData: this.buildFinalSourceFileData(sourceData)
        };
    }
}

export { Backgrounds };
