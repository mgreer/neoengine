import path from "path";
import { BackgroundDef } from "./interfaces";
import { TileBinaryData } from "../entities/interfaces";
import { CROMTile } from "../cromTile";
import { Raw24BitPalette, PaletteColorData } from "../palettes/interfaces";
import { getContextFromImageFile } from "../imageUtils/context";

class Background {
    constructor(private backgroundDef: BackgroundDef) {}

    _tiles: CROMTile[] | null = null;

    private buildCData(tiles: CROMTile[]): TileBinaryData {
        const cOddData = tiles.reduce(
            (buildingCOddData, tile) => {
                if (tile.cOddData) {
                    return buildingCOddData.concat(tile.cOddData);
                } else {
                    throw new Error("Background: tile had no c odd data");
                }
            },
            [] as number[]
        );

        const cEvenData = tiles.reduce(
            (buildingCEvenData, tile) => {
                if (tile.cEvenData) {
                    return buildingCEvenData.concat(tile.cEvenData);
                } else {
                    throw new Error("Backround: tile had no c even data");
                }
            },
            [] as number[]
        );

        return {
            cOddData,
            cEvenData
        };
    }

    private buildHeaderData(backgroundIndex: number) {
        if (!this._tiles) {
            throw new Error(
                "Background#buildHeaderData: called before tiles exist"
            );
        }

        const lowerName = this.backgroundDef.name;
        const upperName = lowerName.toUpperCase();

        return `// start BACKGROUND_${upperName}

#define BACKGROUND_${upperName} ${backgroundIndex}
#define BACKGROUND_${upperName}_TILE_COUNT ${this._tiles.length}

const struct BackgroundDef backgroundDef_${lowerName};

// end BACKGROUND_${upperName}`;
    }

    private buildSourceData(tileMap: Map<CROMTile, number>) {
        if (!this._tiles) {
            throw new Error(
                "Background#buildSourceData: called before tiles exist"
            );
        }

        const lowerName = this.backgroundDef.name;
        const upperName = lowerName.toUpperCase();

        const tileEntries = this._tiles.map(tile => {
            return `{ .index = ${tileMap.get(tile)}, .palette = ${
                tile.paletteIndex
            }, .horizontalMirror = 0, .verticalMirror = 0 }`;
        });

        return `const struct TileDef tileDefs_background_${lowerName}[${
            this._tiles.length
        }] = {
    ${tileEntries.join(",\n    ")}
};

const struct BackgroundDef backgroundDef_${lowerName} = {
    .type = BACKGROUND_${upperName},
    .tileCount = BACKGROUND_${upperName}_TILE_COUNT,
    .tiles = tileDefs_background_${lowerName}
};`;
    }

    getTiles(dir: string): CROMTile[] {
        if (!this._tiles) {
            const context = getContextFromImageFile(
                path.join(dir, this.backgroundDef.file)
            );

            this._tiles = [];

            for (let tx = 0; tx < context.canvas.width; tx += 16) {
                for (let ty = 0; ty < context.canvas.height; ty += 16) {
                    const corners = CROMTile.separateIntoCorners(
                        context,
                        tx,
                        ty
                    );

                    this._tiles.push(new CROMTile(corners));
                }
            }
        }

        return this._tiles;
    }

    buildData(
        indexedPaletteDataMap: Map<Raw24BitPalette, PaletteColorData>,
        tileMap: Map<CROMTile, number>,
        backgroundIndex: number
    ) {
        if (!this._tiles) {
            throw new Error(
                "Background: buildData called before tiles established"
            );
        }

        this._tiles.forEach(tile => {
            const indexedPaletteData = indexedPaletteDataMap.get(
                tile.rgbPalette
            );

            if (!indexedPaletteData) {
                throw new Error(
                    "EntityAnimations#buildData: failed to get indexedPaletteData for a tile"
                );
            }

            tile.index(indexedPaletteData);
            tile.createCData();
        });

        const cData = this.buildCData(this._tiles);
        const headerData = this.buildHeaderData(backgroundIndex);
        const sourceData = this.buildSourceData(tileMap);

        return {
            cData,
            headerData,
            sourceData
        };
    }

    public dumpThenFixBadTiles(dir: string) {
        if (!this._tiles) {
            return;
        }

        this._tiles.forEach((t, i) => {
            if (t.rgbPalette.length > 16) {
                const dumpFileName = `BADTILE_background_${
                    this.backgroundDef.name
                }_tile${i}.png`;
                const dumpPath = path.join(dir, dumpFileName);
                CROMTile.dump(dumpPath, t);
                CROMTile.reduceTo16Colors(t);
            }
        });
    }
}

export { Background };
