export type Raw24BitColor = [number, number, number, number];
export type Packed16BitColor = number;

// a palette always has at least one color
export type Raw24BitPalette = [Raw24BitColor, ...Raw24BitColor[]];
export type Raw16BitPalette = [Packed16BitColor, ...Packed16BitColor[]];
export type Sorted16BitPalette = [Packed16BitColor, ...Packed16BitColor[]];

export type PaletteIndex =
    | 0
    | 1
    | 2
    | 3
    | 4
    | 5
    | 6
    | 7
    | 8
    | 9
    | 10
    | 11
    | 12
    | 13
    | 14
    | 15;

export type PaletteColorData = {
    raw24ColorToIndex: Map<string, PaletteIndex>;
    paletteIndex: number;
};
