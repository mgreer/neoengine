import { uniq, uniqBy, isEqual } from "lodash";
import {
    PaletteIndex,
    PaletteColorData,
    Raw16BitPalette,
    Raw24BitPalette,
    Raw24BitColor,
    Sorted16BitPalette
} from "./interfaces";
import {
    convertFrom24To16Bit,
    convertRaw24To16Palette,
    raw24BitColorToString,
    raw24BitPaletteToString
} from "./colorUtils";

export const TRANSPARENT_24BIT_COLOR: Raw24BitColor = [255, 0, 255, 255];
export const TRANSPARENT_16BIT_COLOR = convertFrom24To16Bit(
    TRANSPARENT_24BIT_COLOR
);

function sortPalette(r16: Raw16BitPalette): Sorted16BitPalette {
    const sortedArray = [...r16];

    sortedArray.sort((a, b) => {
        if (a === TRANSPARENT_16BIT_COLOR) {
            return -1;
        }

        if (b === TRANSPARENT_16BIT_COLOR) {
            return 1;
        }

        return a - b;
    }) as Sorted16BitPalette;

    return uniq(sortedArray) as Sorted16BitPalette;
}

function doesPaletteSatisfy(
    p: Sorted16BitPalette,
    contender: Sorted16BitPalette
): boolean {
    if (contender.length < p.length) {
        return false;
    }

    for (let i = 0; i < p.length; ++i) {
        if (contender.indexOf(p[i]) < 0) {
            return false;
        }
    }

    return true;
}

function isPaletteUnsatisfiable(
    palette: Sorted16BitPalette,
    palettes: Sorted16BitPalette[]
): boolean {
    return !palettes.some(contender => {
        // this means a palette satisfying itself does not count
        // it also means two palettes that are identical to each other don't count
        // otherwise they will create a "loop" and fall out of the final palettes
        if (isEqual(palette, contender)) {
            return false;
        }

        return doesPaletteSatisfy(palette, contender);
    });
}

function mergeTwoPalettes(
    pa: Sorted16BitPalette,
    pb: Sorted16BitPalette
): Sorted16BitPalette {
    const merged = pa.concat(pb);
    return sortPalette(merged as Raw16BitPalette);
}

/**
 * given a set of palettes, combine them into fewer palettes by filling in as
 * many gaps in palettes as possible.
 *
 * For example if palette a is [transparent, a, b]
 * and palette b is [transparent, b, c], they can be combined into [transparent, a, b, c]
 */
function mergeAllPalettes(
    palettes: Sorted16BitPalette[]
): Sorted16BitPalette[] {
    const inputPalettes = [...palettes];
    const mergedPalettes = [];

    for (let i = 0; i < inputPalettes.length; ++i) {
        let merged = false;
        for (let k = i + 1; k < inputPalettes.length; ++k) {
            const mergedPalette = mergeTwoPalettes(
                inputPalettes[i],
                inputPalettes[k]
            );

            if (mergedPalette.length <= 16) {
                mergedPalettes.push(mergedPalette);
                inputPalettes.splice(k, 1);
                merged = true;
                break;
            }
        }

        if (!merged) {
            mergedPalettes.push(inputPalettes[i]);
        }
    }

    return mergedPalettes;
}

/**
 * Keep merging palettes till we can't anymore.
 * Cheap way to merge as much as we can. There is surely a better algorithm...
 */
function mergeAllPalettesRepeatedly(
    palettes: Sorted16BitPalette[]
): Sorted16BitPalette[] {
    let mergedPalettes = mergeAllPalettes(palettes);

    while (mergedPalettes.length < palettes.length) {
        palettes = mergedPalettes;
        mergedPalettes = mergeAllPalettes(palettes);
    }

    return mergedPalettes;
}

function dedupePalettes(
    raw16ToSorted: Map<Raw16BitPalette, Sorted16BitPalette>
): Map<Raw16BitPalette, Sorted16BitPalette> {
    // find all the palettes that no other palette can satisfy -> finalPalettes
    // for each palette, replace it with a finalPalette, possibly replacing it with itself

    const inputPalettes = Array.from(raw16ToSorted.values());

    // final palettes should not have any dupes in it, as it is wasteful
    const unsatisfiablePalettes = uniqBy(
        inputPalettes.filter(p => isPaletteUnsatisfiable(p, inputPalettes)),
        p => p.join(",")
    );

    const finalPalettes = mergeAllPalettesRepeatedly(unsatisfiablePalettes);

    const finalMap: Map<Raw16BitPalette, Sorted16BitPalette> = new Map<
        Raw16BitPalette,
        Sorted16BitPalette
    >();

    raw16ToSorted.forEach((s16p, r16p) => {
        const newPalette = finalPalettes.find(p => doesPaletteSatisfy(s16p, p));

        if (!newPalette) {
            throw new Error(
                "GameAnimations: While deduping palettes failed to find a palette for current"
            );
        }

        finalMap.set(r16p, newPalette);
    });

    return finalMap;
}

export function padAndRenderPalette(
    palette: Sorted16BitPalette,
    zeroConstant: string,
    size: number = 16
): string[] {
    let paddingSize = size - palette.length;

    if (paddingSize < 0) {
        console.error(
            `palette with more than ${size} colors (has ${palette.length})`
        );

        paddingSize = 0;
    }

    const padding = new Array(paddingSize).fill(zeroConstant, 0, paddingSize);
    const paletteAsHexStrings = palette.map(c => `0x${c.toString(16)}`);

    return paletteAsHexStrings.concat(padding);
}

export function validatePalettes(palettes: Raw24BitPalette[]) {
    palettes.forEach(p => {
        if (p.length > 16) {
            console.warn(
                "validatePalettes: raw palette with more than 16 colors, has",
                p.length,
                "colors:",
                raw24BitPaletteToString(p)
            );
        }
    });
}

export function createIndexedPaletteMaps(
    fix24BitPalettes: Raw24BitPalette[],
    sprite24BitPalettes: Raw24BitPalette[]
): {
    indexedPaletteDataMap: Map<Raw24BitPalette, PaletteColorData>;
    final16BitPalettesToWriteToGame: Sorted16BitPalette[];
} {
    // TODO: ensure fix palettes stay within the first 16
    const raw24BitPalettes = fix24BitPalettes.concat(sprite24BitPalettes);

    // step one
    const raw16BitPalettes = raw24BitPalettes.map(r24 =>
        convertRaw24To16Palette(r24)
    );

    // step two
    const raw24PaletteToRaw16Palette: Map<
        Raw24BitPalette,
        Raw16BitPalette
    > = new Map<Raw24BitPalette, Raw16BitPalette>();
    raw16BitPalettes.forEach((r16, i) =>
        raw24PaletteToRaw16Palette.set(raw24BitPalettes[i], r16)
    );

    // step three
    const raw16ToSorted16Palettes: Map<
        Raw16BitPalette,
        Sorted16BitPalette
    > = new Map<Raw16BitPalette, Sorted16BitPalette>();
    raw16BitPalettes.forEach(r16 =>
        raw16ToSorted16Palettes.set(r16, sortPalette(r16))
    );

    // step four
    const raw16ToDedupedPalettes: Map<
        Raw16BitPalette,
        Sorted16BitPalette
    > = dedupePalettes(raw16ToSorted16Palettes);

    // step five
    const final16BitPalettesToWriteToGame = uniqBy(
        Array.from(raw16ToDedupedPalettes.values()),
        p => p.join(",")
    );

    // step six and seven combined
    const bigMap: Map<Raw24BitPalette, PaletteColorData> = new Map<
        Raw24BitPalette,
        PaletteColorData
    >();
    raw24BitPalettes.forEach(r24p => {
        const r16p = raw24PaletteToRaw16Palette.get(r24p);
        if (!r16p) {
            throw new Error("failed to get a r16p from a r24p");
        }
        const sorted16p = raw16ToDedupedPalettes.get(r16p);

        if (!sorted16p) {
            throw new Error("Failed to get a sorted16p from a r16p");
        }

        const matchingPalette = final16BitPalettesToWriteToGame!.find(p =>
            doesPaletteSatisfy(p, sorted16p)
        );
        const paletteIndex = final16BitPalettesToWriteToGame!.indexOf(
            matchingPalette!
        );

        if (paletteIndex < 0) {
            throw new Error("Failed to find a palette index");
        }

        const raw24ColorToIndex: Map<string, PaletteIndex> = new Map<
            string,
            PaletteIndex
        >();

        r16p.forEach((r16c, i) => {
            raw24ColorToIndex.set(
                raw24BitColorToString(r24p[i]),
                sorted16p.indexOf(r16c) as PaletteIndex
            );
        });

        bigMap.set(r24p, { paletteIndex, raw24ColorToIndex });
    });

    return { indexedPaletteDataMap: bigMap, final16BitPalettesToWriteToGame };
}

export function extractRgbPalette(
    rgbImageData: Uint8ClampedArray
): Raw24BitPalette {
    const colors: Raw24BitColor[] = [];

    for (let i = 0; i < rgbImageData.length; i += 4) {
        colors.push(Array.from(rgbImageData.slice(i, i + 4)) as Raw24BitColor);
    }

    // make sure all palettes have magenta/transparent
    colors.unshift([255, 0, 255, 255]);

    return uniqBy(colors, raw24BitColorToString) as Raw24BitPalette;
}

export type PalettesResult = { headerFileData: string; sourceFileData: string };

export function buildPaletteData(
    palettes: Sorted16BitPalette[]
): PalettesResult {
    const paletteZeroConstant = "Z";
    const numPalettesConstant = "NUM_PALETTES";
    const numPaletteEntriesConstant = "NUM_PALETTE_ENTRIES";

    const numEntries = palettes.length * 16;

    const commonRoot = `const u16 palettes[${numPaletteEntriesConstant}]`;

    const headerFileData = `#pragma once
#include <ng/neogeo.h>
    
#define ${paletteZeroConstant} 0
#define ${numPalettesConstant} ${palettes.length}
#define ${numPaletteEntriesConstant} ${numEntries}
${commonRoot};`;

    const palettesData = palettes.map((p: Sorted16BitPalette, i) => {
        const paddedPalette = padAndRenderPalette(p, paletteZeroConstant);
        const entries = paddedPalette.join(", ");
        return `// palette ${i}
    ${entries}`;
    });

    const sourceFileData = `#include "paletteDefs.h"

${commonRoot} = {
${palettesData.join(",\n    ")}
};`;

    return { headerFileData, sourceFileData };
}
