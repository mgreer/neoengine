import {
    Raw24BitColor,
    Packed16BitColor,
    Raw24BitPalette,
    Raw16BitPalette
} from "./interfaces";

/**
 * Convert from 24 bit colorspace to the Neo Geo's 16bit color space
 *
 * This algorithm was stolen from neogeodev.org
 * https://wiki.neogeodev.org/index.php?title=Colors<Paste>
 * Look at the event listener for the convert button
 *
 * Must admit, do not fully understand this algorithm
 */
export function convertFrom24To16Bit(
    raw24BitColor: Raw24BitColor
): Packed16BitColor {
    let [red, green, blue] = raw24BitColor;

    const luma = Math.floor(54.213 * red + 182.376 * green + 18.411 * blue) & 1;

    red = Math.floor(red / 8);
    green = Math.floor(green / 8);
    blue = Math.floor(blue / 8);

    const packed16BitColor =
        ((luma ^ 1) << 15) |
        ((red & 1) << 14) |
        ((green & 1) << 13) |
        ((blue & 1) << 12) |
        ((red & 0x1e) << 7) |
        ((green & 0x1e) << 3) |
        (blue >> 1);

    return packed16BitColor;
}

export function convertRaw24To16Palette(
    raw24BitPalette: Raw24BitPalette
): Raw16BitPalette {
    return raw24BitPalette.map(raw24BitColor =>
        convertFrom24To16Bit(raw24BitColor)
    ) as Raw16BitPalette;
}

export function raw24BitColorToString(r24c: Raw24BitColor): string {
    return `r24c-${r24c.join(",")}`;
}

export function isTransparentColor(r24c: Raw24BitColor): boolean {
    return raw24BitColorToString(r24c) === "r24c-255,0,255,255";
}

export function raw24BitPaletteToString(p: Raw24BitPalette): string {
    return p.map(c => `[${c.join(",")}]`).join(", ");
}
