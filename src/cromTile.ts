import fs from "fs";
import {
    Raw24BitColor,
    Raw24BitPalette,
    PaletteColorData
} from "./palettes/interfaces";
import {
    raw24BitColorToString,
    isTransparentColor
} from "./palettes/colorUtils";
import { TRANSPARENT_24BIT_COLOR } from "./palettes";
import { createInMemoryContext, copyImageData } from "./imageUtils/context";
import { uniqBy, isEqual } from "lodash";

class CROMTile {
    constructor(private corners: Uint8ClampedArray[]) {}

    private _rgbPalette: Raw24BitPalette | null = null;

    indexedCorners: number[][] | null = null;
    cOddData: number[] | null = null;
    cEvenData: number[] | null = null;
    paletteIndex: number = -1;

    private extractRgbPalette(corners: Uint8ClampedArray[]) {
        const colors: Raw24BitColor[] = [];

        corners.forEach(corner => {
            for (let i = 0; i < corner.length; i += 4) {
                colors.push(Array.from(
                    corner.slice(i, i + 4)
                ) as Raw24BitColor);
            }
        });

        return uniqBy(colors, raw24BitColorToString) as Raw24BitPalette;
    }

    private convertToIndexed(
        rawImageData: Uint8ClampedArray,
        indexedPaletteData: PaletteColorData
    ) {
        const indexed: number[] = [];

        for (let i = 0; i < rawImageData.length; i += 4) {
            const currentColor: Raw24BitColor = Array.from(
                rawImageData.slice(i, i + 4)
            ) as Raw24BitColor;

            const index = indexedPaletteData.raw24ColorToIndex.get(
                raw24BitColorToString(currentColor)
            );

            if (index === undefined) {
                throw new Error(
                    "Sprite Tile: Convert a tile corner to indexed, failed to find a color"
                );
            }

            indexed.push(index);
        }

        return indexed;
    }

    private getBytesForIndexedCorner(
        indexedCorner: number[],
        planeOffset: 0 | 2
    ): number[] {
        // we have a 1d array of indexed colors [0, 1, 2, 15, 0, 1, ...]
        // the indexed values are 4 bit (max 15)
        // we need to grab eight bytes from the corner, and convert it into 2 bytes:
        //  -- first byte, the first bit of the eight pixels (offset by planeOffset)
        //  -- second byte, the second bit of the eight pixels (offset by planeOffset)

        const planeBytes: number[] = [];

        for (let i = 0; i < indexedCorner.length; i += 8) {
            let firstPlaneByte = 0;
            let secondPlaneByte = 0;

            for (let p = 0; p < 8; ++p) {
                const indexedValue = indexedCorner[i + p];
                firstPlaneByte |= ((indexedValue >> planeOffset) & 1) << p;
                secondPlaneByte |=
                    ((indexedValue >> (planeOffset + 1)) & 1) << p;
            }

            planeBytes.push(firstPlaneByte, secondPlaneByte);
        }

        return planeBytes;
    }

    private getBytesForIndexedCorners(
        indexedCorners: number[][],
        planeOffset: 0 | 2
    ): number[] {
        return indexedCorners.reduce(
            (buildingBytes, corner) => {
                return buildingBytes.concat(
                    this.getBytesForIndexedCorner(corner, planeOffset)
                );
            },
            [] as number[]
        );
    }

    private regetRgbPalette() {
        this._rgbPalette = this.extractRgbPalette(this.corners);
    }

    get rgbPalette(): Raw24BitPalette {
        if (!this._rgbPalette) {
            this._rgbPalette = this.extractRgbPalette(this.corners);
        }

        return this._rgbPalette;
    }

    isEmpty(): boolean {
        return (
            this.rgbPalette.length === 1 &&
            isEqual(this.rgbPalette[0], TRANSPARENT_24BIT_COLOR)
        );
    }

    createCData() {
        if (!this.indexedCorners) {
            throw new Error(
                "Sprite Tile#createCData: called before indexedCorners were created"
            );
        }

        this.cOddData = this.getBytesForIndexedCorners(this.indexedCorners, 0);
        this.cEvenData = this.getBytesForIndexedCorners(this.indexedCorners, 2);
    }

    index(indexedPaletteData: PaletteColorData) {
        this.paletteIndex = indexedPaletteData.paletteIndex;

        this.indexedCorners = this.corners.map(corner =>
            this.convertToIndexed(corner, indexedPaletteData)
        );
    }

    static separateIntoCorners(
        imgContext: CanvasRenderingContext2D,
        x: number,
        y: number
    ): Uint8ClampedArray[] {
        return [
            // upper right comes first
            imgContext.getImageData(x + 8, y + 0, 8, 8).data,
            // then lower right
            imgContext.getImageData(x + 8, y + 8, 8, 8).data,
            // then upper left
            imgContext.getImageData(x, y, 8, 8).data,
            // then lower left
            imgContext.getImageData(x, y + 8, 8, 8).data
        ];
    }

    static getInvalidlySizedTiles(tiles: CROMTile[]): CROMTile[] {
        return tiles.filter(tile => {
            return tile.corners.some(corner => {
                return corner.length !== 64 * 4;
            });
        });
    }

    static dump(dumpPath: string, tile: CROMTile) {
        const context = createInMemoryContext(16, 16);
        const imageData = context.createImageData(8, 8);

        const cornerCoords = [[8, 0], [8, 8], [0, 0], [0, 8]];

        tile.corners.forEach((c, i) => {
            copyImageData(c, imageData);
            context.putImageData(
                imageData,
                cornerCoords[i][0],
                cornerCoords[i][1]
            );
        });

        const out = fs.createWriteStream(dumpPath);
        //@ts-ignore
        const pngStream = context.canvas.createPNGStream();
        pngStream.pipe(out);

        out.on("finish", () => console.log("Bad tile dumped to", dumpPath));
    }

    /**
     * Takes a tile that has too many colors and reduces it to sixteen. This is
     * done by find the colors that are closest to each other in value and merging
     * until there is 16 colors remaining
     */
    static reduceTo16Colors(tile: CROMTile) {
        debugger;
        const palette = tile.rgbPalette;
        const numberColorsToRemove = palette.length - 16;

        console.log(
            "About to remove",
            numberColorsToRemove,
            "colors from a tile"
        );

        type ClosestData = { closestColor: Raw24BitColor; distance: number };

        function getDistance(a: Raw24BitColor, b: Raw24BitColor): number {
            return a.reduce((buildingDistance, channel, index) => {
                return buildingDistance + channel - b[index];
            }, 0);
        }

        function getClosestColorAndDistance(
            c: Raw24BitColor,
            p: Raw24BitPalette
        ): ClosestData {
            const closestColor = p.reduce((best, contender) => {
                if (best === c) {
                    return contender;
                }

                if (contender === c) {
                    return best;
                }

                const bestDistance = getDistance(best, c);
                const contenderDistance = getDistance(contender, c);

                if (Math.abs(bestDistance) < Math.abs(contenderDistance)) {
                    return best;
                } else {
                    return contender;
                }
            });

            return {
                closestColor,
                distance: getDistance(closestColor, c)
            };
        }

        const closestDataMap = new Map<Raw24BitColor, ClosestData>();

        // for each color in the palete, find its closest color that is not itself,
        // and how far it is from that color. The colors with minimal distance from their
        // nearest colors will get purged
        palette.forEach(c => {
            const result: ClosestData = getClosestColorAndDistance(c, palette);
            closestDataMap.set(c, result);
        });

        const entries = Array.from(closestDataMap.entries());

        // this should sort like...
        // [1, 2, 3, -1, -2, -3]
        // but also sort the transparent color to the bottom
        entries.sort((a, b) => {
            if (isTransparentColor(a[0])) {
                return 1;
            }

            if (isTransparentColor(b[0])) {
                return -1;
            }

            // positive numbers come before negative
            if (a[1].distance > 0 && b[1].distance < 0) {
                return -1;
            }

            if (b[1].distance > 0 && a[1].distance < 0) {
                return 1;
            }

            return Math.abs(a[1].distance) - Math.abs(b[1].distance);
        });

        // build a map from color in the current palette to its new color
        // for most colors their entry will point to themselves, but for removed
        // colors, their entry will point to the color they are converting to
        const colorMap = new Map<string, Raw24BitColor>();

        const removedColors: Raw24BitColor[] = [];

        for (let i = 0; i < entries.length; ++i) {
            const currentColor = entries[i][0];
            const replacementColor = entries[i][1].closestColor;

            if (
                removedColors.indexOf(currentColor) < 0 &&
                removedColors.indexOf(replacementColor) < 0 &&
                removedColors.length < numberColorsToRemove
            ) {
                colorMap.set(
                    raw24BitColorToString(currentColor),
                    replacementColor
                );
                removedColors.push(currentColor);
            } else {
                colorMap.set(raw24BitColorToString(currentColor), currentColor);
            }
        }

        tile.corners.forEach(corner => {
            for (let i = 0; i < corner.length; i += 4) {
                const colorToConsider = Array.from(
                    corner.slice(i, i + 4)
                ) as Raw24BitColor;
                const colorAsString = raw24BitColorToString(colorToConsider);
                const replacementColor = colorMap.get(colorAsString);

                if (!replacementColor) {
                    throw new Error(
                        "CROMTile#reduceTo16Colors: failed to find a replacementColor"
                    );
                }

                const replacementColorAsString = raw24BitColorToString(
                    replacementColor
                );

                if (replacementColorAsString !== colorAsString) {
                    replacementColor.forEach((channel, index) => {
                        corner[i + index] = channel;
                    });
                }
            }
        });

        tile.regetRgbPalette();

        console.log(
            "after remove colors, tile now has",
            tile.rgbPalette.length,
            "colors"
        );
    }
}

export { CROMTile };
