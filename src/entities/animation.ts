import path from "path";
import { AnimationDef } from "./interfaces";
import {
    Raw24BitColor,
    Raw24BitPalette,
    PaletteColorData
} from "../palettes/interfaces";
import { uniqBy, isEqual } from "lodash";
import { raw24BitColorToString } from "../palettes/colorUtils";
import { CROMTile } from "../cromTile";
import { getContextFromImageFile } from "../imageUtils/context";

const TILE_WIDTH = 16;
const TILE_HEIGHT = TILE_WIDTH;
const CORNER_WIDTH = TILE_WIDTH / 2;
const CORNER_HEIGHT = TILE_HEIGHT / 2;

// magenta indicates the color should be transparent
const TRANSPARENT_COLOR: Raw24BitColor = [255, 0, 255, 255];

function areColorsEqual(colorA: Raw24BitColor, colorB: Raw24BitColor): boolean {
    return colorA.every((channel, index) => channel === colorB[index]);
}

export class Frame {
    constructor(public tiles: CROMTile[]) {}

    static isEmpty(frame: Frame): boolean {
        return frame.tiles.every(tile => {
            return tile.isEmpty();
        });
    }

    getTiles(): CROMTile[] {
        return this.tiles;
    }

    public dumpThenFixBadTiles(dumpDir: string, dumpName: string) {
        this.tiles.forEach((t, i) => {
            if (t.rgbPalette.length > 16) {
                const dumpFileName = `BADTILE_${dumpName}_tile${i}.png`;
                const dumpPath = path.join(dumpDir, dumpFileName);
                CROMTile.dump(dumpPath, t);
                CROMTile.reduceTo16Colors(t);
            }
        });
    }
}

export class Animation {
    private _frames: Frame[] = [];
    private haveAllFrames: boolean = false;

    constructor(
        public animationDef: AnimationDef,
        private dir: string,
        private defaultFileName: string | undefined
    ) {}

    private getAseJson(): { [key: string]: any } {
        if (!this.animationDef.aseJson) {
            throw new Error(
                "Animation: getAseJson called but no aseJson is defined"
            );
        }
        const aseJsonPath = path.join(this.dir, this.animationDef.aseJson);
        return require(aseJsonPath);
    }

    public dumpThenFixBadTiles(dumpDir: string, entityName: string) {
        this._frames.forEach((f, i) => {
            f.dumpThenFixBadTiles(
                dumpDir,
                `${entityName}_${this.animationDef.name}_frame${i}`
            );
        });
    }

    public getTileWidthAndHeight(): { width: number; height: number } {
        const { frameWidth, frameHeight } = this.getFrameWidthAndHeight();

        return {
            width: frameWidth / 16,
            height: frameHeight / 16
        };
    }

    public getFrameWidthAndHeight(): {
        frameWidth: number;
        frameHeight: number;
    } {
        if (this.animationDef.width && this.animationDef.height) {
            if (this.animationDef.aseJson) {
                throw new Error(
                    "An animation definition should not have width/height and aseJson, they are either/or"
                );
            }

            return {
                frameWidth: this.animationDef.width * 16,
                frameHeight: this.animationDef.height * 16
            };
        } else if (this.animationDef.aseJson) {
            const aseJson = this.getAseJson();

            const frameKey = Object.keys(aseJson.frames)[0];
            const frameData = aseJson.frames[frameKey].frame;

            return { frameWidth: frameData.w, frameHeight: frameData.h };
        } else {
            throw new Error(
                "Invalid animation definition, does not have width/height or aseJson"
            );
        }
    }

    public getDuration(frameIndex: number) {
        if (this.animationDef.aseJson) {
            const aseJson = this.getAseJson();

            const frameKey = Object.keys(aseJson.frames)[frameIndex];
            const frameData = aseJson.frames[frameKey];

            // convert from milliseconds to Neo Geo frame duration
            return Math.floor(frameData.duration / (1000 / 60));
        } else {
            return (
                (this.animationDef.durations &&
                    this.animationDef.durations[frameIndex]) ||
                10
            );
        }
    }

    private getImageY(): number {
        if (typeof this.animationDef.fileOffset === "number") {
            return this.animationDef.fileOffset * 16;
        } else {
            return 0;
        }
    }

    private getContext(): CanvasRenderingContext2D {
        let filePath;

        if (this.animationDef.aseJson) {
            const aseJson = this.getAseJson();
            filePath = aseJson.meta.image;
        } else if (this.animationDef.file) {
            filePath = path.join(this.dir, this.animationDef.file);
        } else if (this.defaultFileName) {
            filePath = path.join(this.dir, this.defaultFileName);
        } else {
            throw new Error(
                "Animation: animation definition has no image file"
            );
        }

        return getContextFromImageFile(filePath);
    }

    private extractFrames() {
        if (this._frames.length) {
            return this._frames;
        }

        const { frameWidth, frameHeight } = this.getFrameWidthAndHeight();
        const y = this.getImageY();

        const context = this.getContext();

        for (let x = 0; x < context.canvas.width; x += frameWidth) {
            if (context.canvas.width - x < frameWidth) {
                console.warn(
                    `Not enough image width data for this frame, assuming it is not a frame: ${
                        this.animationDef.name
                    }`
                );
                continue;
            }

            if (context.canvas.height - y < frameHeight) {
                console.warn(
                    `Not enough image height data for this frame, assuming it is not a frame: ${
                        this.animationDef.name
                    }`
                );
                continue;
            }

            let frameData: CROMTile[] = [];

            for (let cx = x; cx < x + frameWidth; cx += 16) {
                const tiles: CROMTile[] = [];

                for (let cy = y; cy < y + frameHeight; cy += 16) {
                    const corners = CROMTile.separateIntoCorners(
                        context,
                        cx,
                        cy
                    );
                    tiles.push(new CROMTile(corners));
                }
                frameData = frameData.concat(tiles);
            }

            const frame = new Frame(frameData);

            if (Frame.isEmpty(frame)) {
                break;
            } else {
                this._frames.push(frame);
            }
        }
    }

    getEnum(entityName: string, value: number) {
        return `#define ${entityName}_${this.animationDef.name} ${value}`;
    }

    getAnimationArrayHeader(entityName: string) {
        return `const u16 ${entityName}_${this.animationDef.name}_frames[];`;
    }

    getAnimationArrayData(entityName: string) {
        const header = this.getAnimationArrayHeader(entityName).replace(
            ";",
            " = {"
        );

        // dump the tile data into a 2d array here
    }

    get numberOfFrames(): number {
        return (this._frames && this._frames.length) || 0;
    }

    get frames(): Frame[] {
        if (!this._frames.length) {
            this.extractFrames();
        }

        return this._frames;
    }
}
