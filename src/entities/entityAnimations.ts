import { EntityDef, AnimationDef, TileBinaryData } from "./interfaces";
import { Raw24BitPalette, PaletteColorData } from "../palettes/interfaces";
import { raw24BitPaletteToString } from "../palettes/colorUtils";
import { Animation, Frame } from "./animation";
import { CROMTile } from "../cromTile";

export type EntityAnimationsProcessResult = {
    cData: TileBinaryData;
    headerData: string;
    sourceData: string;
};

function isAnimationDef(
    obj: AnimationDef | AnimationDef[]
): obj is AnimationDef {
    return (
        !Array.isArray(obj) &&
        typeof obj.width === "number" &&
        typeof obj.height === "number"
    );
}

export class EntityAnimations {
    private tiles: CROMTile[] | null = null;
    private animations: Animation[] = [];
    private tileMap: Map<CROMTile, number> = new Map();

    private buildCData(tiles: CROMTile[]): TileBinaryData {
        const cOddData = tiles.reduce(
            (buildingCOddData, tile) => {
                if (tile.cOddData) {
                    return buildingCOddData.concat(tile.cOddData);
                } else {
                    throw new Error("EntityAnimation: tile had no c data");
                }
            },
            [] as number[]
        );

        const cEvenData = tiles.reduce(
            (buildingCEvenData, tile) => {
                if (tile.cEvenData) {
                    return buildingCEvenData.concat(tile.cEvenData);
                } else {
                    throw new Error("EntityAnimation: tile had no c data");
                }
            },
            [] as number[]
        );

        return {
            cOddData,
            cEvenData
        };
    }

    private getAnimationEnums(): string {
        const upperName = this.entityDef.name.toUpperCase();

        const enums = this.entityDef.animations.map((a, i) => {
            const animationName = (a.name || "default").toUpperCase();
            return `#define ANIMATION_${upperName}_${animationName} ${i}`;
        });

        const frameCountEnums = this.animations.map(a => {
            const animationName = (
                a.animationDef.name || "default"
            ).toUpperCase();
            return `#define FRAMES_${upperName}_${animationName}_COUNT ${
                a.frames.length
            }`;
        });

        return enums.concat(frameCountEnums).join("\n");
    }

    private getTileStructArrays(): string {
        const lowerName = this.entityDef.name;
        const upperName = lowerName.toUpperCase();

        const tileStructs = this.animations.reduce(
            (buildingTileStructs, a) => {
                const animationName = a.animationDef.name || "default";

                const animationTileStructs = a.frames.reduce(
                    (buildingAniTileStructs, frame, frameIndex) => {
                        const frameTileStructs = frame.tiles.map(tile => {
                            return `{ .index = ${this.tileMap.get(
                                tile
                            )}, .palette = ${
                                tile.paletteIndex
                            }, .horizontalMirror = 0, .verticalMirror = 0 }`;
                        });

                        const finalTileArray = `const struct TileDef tileDefs_${lowerName}_${animationName}_for_frame_${frameIndex}[${
                            frame.tiles.length
                        }] = {
    ${frameTileStructs.join(",\n    ")}
};`;

                        return buildingAniTileStructs.concat(finalTileArray);
                    },
                    [] as string[]
                );

                return buildingTileStructs.concat(animationTileStructs);
            },
            [] as string[]
        );

        return tileStructs.join("\n");
    }

    private getFrameStructs(a: Animation): string {
        const lowerName = this.entityDef.name;

        const animationName = a.animationDef.name || "default";

        const animationFrameStructs = a.frames.map((frame, i) => {
            return `{
    .tiles = tileDefs_${lowerName}_${animationName}_for_frame_${i},
    .width = ${a.getTileWidthAndHeight().width},
    .height = ${a.getTileWidthAndHeight().height},
    .duration = ${a.getDuration(i)},
    .durationRandomMask = ${(a.animationDef.durationRandomMasks &&
        a.animationDef.durationRandomMasks[i]) ||
        0}
}`;
        });

        return animationFrameStructs.join(",\n");
    }

    private getFrameArrays(): string {
        const lowerName = this.entityDef.name;
        const upperName = lowerName.toUpperCase();

        const frameArrays = this.animations.map(a => {
            const animationName = a.animationDef.name || "default";
            const upperAnimationName = animationName.toUpperCase();

            const entries = this.getFrameStructs(a);

            return `const struct FrameDef frameDefs_${lowerName}_${animationName}[FRAMES_${upperName}_${upperAnimationName}_COUNT] = {
    ${entries}
};`;
        });

        return frameArrays.join("\n");
    }

    private getAnimationStructs(): string {
        const lowerName = this.entityDef.name;

        const structs = this.animations.map(a => {
            const animationName = a.animationDef.name || "default";
            return `{
    .frames = frameDefs_${lowerName}_${animationName},
    .frameCount = ${a.frames.length},
    .loop = ${a.animationDef.loop ? "1" : "0"},
    .loopFrame = ${a.animationDef.loopFrame || "0"},
    .offsetX = ${a.animationDef.offsetX || 0},
    .offsetY = ${a.animationDef.offsetY || 0}
}`;
        });

        return structs.join(",\n");
    }

    private getAnimationArrays(): string {
        const lowerName = this.entityDef.name;

        const animationEntries = this.getAnimationStructs();

        return `const struct AnimationDef animationDefs_${lowerName}[${
            this.animations.length
        }] = {
    ${animationEntries}
};`;
    }

    private getMaxTileWidth(): number {
        return this.animations.reduce((best, a) => {
            const animationWidth = a.getFrameWidthAndHeight().frameWidth;

            return Math.max(animationWidth, best);
        }, 0);
    }

    private getMaxTileHeight(): number {
        return this.animations.reduce((best, a) => {
            const animationHeight = a.getFrameWidthAndHeight().frameHeight;

            return Math.max(animationHeight, best);
        }, 0);
    }

    private buildHeaderData(entityIndex: number): string {
        const lowerName = this.entityDef.name;
        const upperName = lowerName.toUpperCase();

        return `
// start ENTITY_${upperName}
#define ENTITY_${upperName} ${entityIndex}

${this.getAnimationEnums()}

const struct EntityGraphicDef ${lowerName}_entityGraphicDef;

// end ENTITY_${upperName}
`;
    }

    private buildSourceData(entityIndex: number): string {
        const lowerName = this.entityDef.name;
        const upperName = lowerName.toUpperCase();

        return `
${this.getTileStructArrays()}
${this.getFrameArrays()}
${this.getAnimationArrays()}

const struct EntityGraphicDef ${lowerName}_entityGraphicDef = {
    .type = ENTITY_${upperName},
    .animations = animationDefs_${lowerName},
    .animationCount = ${this.animations.length},
    .maxWidth = ${this.getMaxTileWidth()},
    .maxHeight = ${this.getMaxTileHeight()},
};`;
    }

    constructor(public entityDef: EntityDef) {}

    getTiles(dir: string): CROMTile[] {
        if (!this.tiles) {
            const animationDefs = this.entityDef.animations;

            this.tiles = [] as CROMTile[];

            let y = 0;
            this.animations = animationDefs.map(animationDef => {
                const animation = new Animation(
                    animationDef,
                    dir,
                    this.entityDef.file
                );

                const frames = animation.frames;

                animation.dumpThenFixBadTiles(dir, this.entityDef.name);

                console.log(
                    "Processing",
                    this.entityDef.name,
                    animationDef.name,
                    "frames:",
                    frames.length
                );

                const frameTiles = frames.reduce(
                    (buildingFrameTiles, frame) => {
                        return buildingFrameTiles.concat(frame.getTiles());
                    },
                    [] as CROMTile[]
                );

                this.tiles = this.tiles!.concat(frameTiles);

                return animation;
            });
        }

        return this.tiles;
    }

    buildData(
        indexedPaletteDataMap: Map<Raw24BitPalette, PaletteColorData>,
        tileOffset: number,
        entityIndex: number
    ): EntityAnimationsProcessResult {
        if (!this.tiles) {
            throw new Error(
                "EntityAnimation: buildData called before tiles established"
            );
        }

        let frameIndex = 0;
        this.tiles.forEach(tile => {
            const indexedPaletteData = indexedPaletteDataMap.get(
                tile.rgbPalette
            );

            if (!indexedPaletteData) {
                throw new Error(
                    "EntityAnimations#buildData: failed to get indexedPaletteData for a tile"
                );
            }

            tile.index(indexedPaletteData);
            tile.createCData();
            this.tileMap.set(tile, tileOffset + frameIndex++);
        });

        const cData = this.buildCData(this.tiles);
        const headerData = this.buildHeaderData(entityIndex);
        const sourceData = this.buildSourceData(entityIndex);

        return {
            cData,
            headerData,
            sourceData
        };
    }
}
