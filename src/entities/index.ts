import { EntityDef, TileBinaryData } from "./interfaces";
import {
    Raw24BitPalette,
    PaletteColorData,
    Sorted16BitPalette
} from "../palettes/interfaces";
import { createIndexedPaletteMaps, padAndRenderPalette } from "../palettes";
import { EntityAnimations } from "./entityAnimations";
import { EyecatcherResult } from "../eyecatcher";
import { CROMTile } from "../cromTile";
import { uniq, uniqBy, isEqual } from "lodash";

export type EntitiesResult = {
    tileCount: number;
    cData: TileBinaryData;
    headerFileData: string;
    sourceFileData: string;
};

class Entities {
    entityAnimations: EntityAnimations[];
    final16BitPalettesToWriteToGame: Sorted16BitPalette[] | null = null;

    private buildFinalCData(tileData: TileBinaryData[]): TileBinaryData {
        const cromData = tileData.reduce(
            (buildingCROMData, tileData) => {
                buildingCROMData.cOddData = buildingCROMData.cOddData.concat(
                    tileData.cOddData
                );
                buildingCROMData.cEvenData = buildingCROMData.cEvenData.concat(
                    tileData.cEvenData
                );

                return buildingCROMData;
            },
            {
                cOddData: [] as number[],
                cEvenData: [] as number[]
            }
        );

        return cromData;
    }

    private buildFinalHeaderFileData(headerData: string[]): string {
        const headerFileSrc = `#include "entityGraphic.h"
${headerData.join("\n\n")}`;

        return headerFileSrc;
    }

    private buildFinalSourceFileData(sourceData: string[]): string {
        const sourceFileSrc = `#include "entityGraphicDefs.h"
${sourceData.join("\n\n")}`;

        return sourceFileSrc;
    }

    private buildPaletteData(): { headerData: string; sourceData: string } {
        if (!this.final16BitPalettesToWriteToGame) {
            throw new Error(
                "GameAnimations#buildPaletteData: called before final palettes were created"
            );
        }

        const paletteZeroConstant = "Z";
        const numPalettesConstant = "NUM_SPRITE_PALETTES";
        const numPaletteEntriesConstant = "NUM_SPRITE_PALETTE_ENTRIES";

        const numEntries = this.final16BitPalettesToWriteToGame.length * 16;

        const commonRoot = `const u16 sprite_palettes[${numPaletteEntriesConstant}]`;

        const headerData = `#define ${paletteZeroConstant} 0
#define ${numPalettesConstant} ${this.final16BitPalettesToWriteToGame.length}
#define ${numPaletteEntriesConstant} ${numEntries}
${commonRoot};`;

        const palettesData = this.final16BitPalettesToWriteToGame.map(
            (p: Sorted16BitPalette, i) => {
                const paddedPalette = padAndRenderPalette(
                    p,
                    paletteZeroConstant
                );
                const entries = paddedPalette.join(", ");
                return `// palette ${i}
    ${entries}`;
            }
        );

        const sourceData = `${commonRoot} = {
${palettesData.join(",\n    ")}
};`;

        return { headerData, sourceData };
    }

    constructor(entityDefs: EntityDef[], private dir: string) {
        this.entityAnimations = entityDefs.map(ed => new EntityAnimations(ed));
    }

    getTiles(): CROMTile[] {
        return this.entityAnimations.reduce(
            (buildingTiles, ea) => {
                const tiles = ea.getTiles(this.dir);
                const invalidTiles = CROMTile.getInvalidlySizedTiles(tiles);

                if (invalidTiles.length > 0) {
                    throw new Error(
                        `Found ${
                            invalidTiles.length
                        } invalidly sized tiles for ${ea.entityDef.name}`
                    );
                }

                return buildingTiles.concat(tiles);
            },
            [] as CROMTile[]
        );
    }

    buildData(
        indexedPaletteDataMap: Map<Raw24BitPalette, PaletteColorData>,
        tileOffset: number
    ): EntitiesResult {
        const entityResults = this.entityAnimations.map((ea, i) => {
            const entityResult = ea.buildData(
                indexedPaletteDataMap,
                tileOffset,
                i
            );
            tileOffset += entityResult.cData.cEvenData.length / 64;

            return entityResult;
        });

        const headerData = entityResults.map(er => er.headerData);

        const sourceData = entityResults.map(er => er.sourceData);
        const cData = this.buildFinalCData(entityResults.map(er => er.cData));

        return {
            tileCount: cData.cOddData.length / 64,
            cData,
            headerFileData: this.buildFinalHeaderFileData(headerData),
            sourceFileData: this.buildFinalSourceFileData(sourceData)
        };
    }
}

export { Entities };
