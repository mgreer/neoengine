export interface AnimationDef {
    // common properties
    name?: string;
    file?: string;
    fileOffset?: number;
    loop?: boolean;
    loopFrame?: number;
    offsetX?: number;
    offsetY?: number;
    durationRandomMasks?: number[];

    // properties for matrix pngs
    width?: number;
    height?: number;
    durations?: number[];

    // properties for ase json inputs
    aseJson?: string;
}

export interface EntityDef {
    name: string;
    file?: string;
    animations: AnimationDef[];
}

export interface TileBinaryData {
    cOddData: number[];
    cEvenData: number[];
}
