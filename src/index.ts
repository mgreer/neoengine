#!/usr/bin/env node

import program from "commander";
import path from "path";
import fs from "fs";

import { Eyecatcher, EyecatcherResult } from "./eyecatcher";
import { Entities, EntitiesResult } from "./entities";
import { StaticFixes, StaticFixProcessResult } from "./staticFix";
import { Backgrounds, BackgroundsResult } from "./backgrounds";

import {
    validatePalettes,
    createIndexedPaletteMaps,
    buildPaletteData,
    PalettesResult
} from "./palettes";
import { Sorted16BitPalette } from "./palettes/interfaces";

const packageJson = require("../package.json");

program
    .version(packageJson.version)
    .option(
        "-i, --input <JSON specification file>",
        "The JSON file specifying everything for input"
    )
    .option(
        "-h, --header-dest <dest directory>",
        "The directory the header files will get written to"
    )
    .option(
        "-r, --rom-dest <dest directory>",
        "The directory the ROM binaries (C and S) will get written to"
    )
    .parse(process.argv);

if (!program.input || !program.headerDest || !program.romDest) {
    program.help();
}

// TODO: this is where we can split the data into multiple C ROM pairs
// once the game outgrows a single pair
function buildFinalSpriteData(
    eyecatcherResult: EyecatcherResult,
    entitiesResult: EntitiesResult,
    backgroundsResult: BackgroundsResult
) {
    return {
        cOddData: eyecatcherResult.cOddData.concat(
            entitiesResult.cData.cOddData,
            backgroundsResult.cData.cOddData
        ),
        cEvenData: eyecatcherResult.cEvenData.concat(
            entitiesResult.cData.cEvenData,
            backgroundsResult.cData.cEvenData
        ),
        headerFileData: entitiesResult.headerFileData,
        sourceFileData: entitiesResult.sourceFileData
    };
}

function writeCData(headerDir: string, romDir: string, spriteData: any) {
    const cOddPath = path.join(romDir, "output.c1");
    fs.writeFileSync(cOddPath, Buffer.from(spriteData.cOddData));

    const cEvenPath = path.join(romDir, "output.c2");
    fs.writeFileSync(cEvenPath, Buffer.from(spriteData.cEvenData));

    const headerPath = path.join(headerDir, "entityGraphicDefs.h");
    fs.writeFileSync(headerPath, spriteData.headerFileData);

    const sourcePath = path.join(headerDir, "entityGraphicDefs.c");
    fs.writeFileSync(sourcePath, spriteData.sourceFileData);

    console.log(
        "entities written to ",
        "\n",
        [cOddPath, cEvenPath, headerPath, sourcePath].join("\n"),
        "\n"
    );
}

function writeFixData(
    headerDir: string,
    romDir: string,
    fixData: StaticFixProcessResult
) {
    const sPath = path.join(romDir, "output.s1");
    fs.writeFileSync(sPath, Buffer.from(fixData.sData.sData));

    const headerPath = path.join(headerDir, "staticFixDefs.h");
    fs.writeFileSync(headerPath, fixData.headerFileData);

    const sourcePath = path.join(headerDir, "staticFixDefs.c");
    fs.writeFileSync(sourcePath, fixData.sourceFileData);

    console.log(
        "fix data written to",
        "\n",
        [sPath, headerPath, sourcePath].join("\n"),
        "\n"
    );
}

function writePaletteData(dir: string, paletteData: PalettesResult) {
    const headerPath = path.join(dir, "paletteDefs.h");
    fs.writeFileSync(headerPath, paletteData.headerFileData);

    const sourcePath = path.join(dir, "paletteDefs.c");
    fs.writeFileSync(sourcePath, paletteData.sourceFileData);

    console.log(
        "palette data written to",
        "\n",
        [headerPath, sourcePath].join("\n"),
        "\n"
    );
}

function writeBackgroundData(dir: string, backgroundData: BackgroundsResult) {
    const headerPath = path.join(dir, "backgroundDefs.h");
    fs.writeFileSync(headerPath, backgroundData.headerFileData);

    const sourcePath = path.join(dir, "backgroundDefs.c");
    fs.writeFileSync(sourcePath, backgroundData.sourceFileData);

    console.log(
        "background data written to",
        "\n",
        [headerPath, sourcePath].join("\n"),
        "\n"
    );
}

const inputJsonPath = path.join(process.cwd(), program.input);
const inputJsonDir = path.dirname(inputJsonPath);

const outputRomDir = path.join(process.cwd(), program.romDest);
const outputHeaderDir = path.join(process.cwd(), program.headerDest);

const resourceJson = require(inputJsonPath);

const eyecatcher = new Eyecatcher(
    path.join(inputJsonDir, resourceJson.eyecatcher)
);
const backgrounds = new Backgrounds(
    resourceJson.backgrounds || [],
    inputJsonDir
);
const entities = new Entities(resourceJson.entities || [], inputJsonDir);
const staticFixes = new StaticFixes(resourceJson.staticFix || [], inputJsonDir);

const allSpriteTiles = eyecatcher
    .getTiles()
    .concat(entities.getTiles())
    .concat(backgrounds.getTiles());
const allFixTiles = staticFixes.getTiles();

const spriteRgbPalettes = allSpriteTiles.map(t => t.rgbPalette);
const fixRgbPalettes = allFixTiles.map(t => t.rgbPalette);

const {
    indexedPaletteDataMap,
    final16BitPalettesToWriteToGame
} = createIndexedPaletteMaps(fixRgbPalettes, spriteRgbPalettes);

const eyecatcherResult = eyecatcher.buildData(indexedPaletteDataMap, 0);
const entitiesResult = entities.buildData(
    indexedPaletteDataMap,
    eyecatcherResult.tileCount
);
const backgroundsResult = backgrounds.buildData(
    indexedPaletteDataMap,
    eyecatcherResult.tileCount + entitiesResult.tileCount
);
const staticFixResult = staticFixes.buildData(indexedPaletteDataMap, 0);
const finalCData = buildFinalSpriteData(
    eyecatcherResult,
    entitiesResult,
    backgroundsResult
);
const finalPaletteData = buildPaletteData(final16BitPalettesToWriteToGame);

writeCData(outputHeaderDir, outputRomDir, finalCData);
writeFixData(outputHeaderDir, outputRomDir, staticFixResult);
writePaletteData(outputHeaderDir, finalPaletteData);
writeBackgroundData(outputHeaderDir, backgroundsResult);
