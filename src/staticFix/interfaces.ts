export interface StaticFixDef {
    name: string;
    file: string;
}

export interface SROMFileBinaryData {
    index: number;
    sData: number[];
}

export interface FixTileBinaryData {
    staticFixDef: StaticFixDef;
    sData: number[];
}
