import fs from "fs";
import path from "path";
import { Canvas, Image } from "canvas";
import { StaticFixDef, FixTileBinaryData } from "./interfaces";
import {
    Raw24BitColor,
    Raw24BitPalette,
    PaletteColorData
} from "../palettes/interfaces";
import { raw24BitColorToString } from "../palettes/colorUtils";
import { extractRgbPalette } from "../palettes";
import { uniqBy } from "lodash";
import { FixTile } from "../fixTile";

class StaticFixImage {
    tiles: FixTile[] | null = null;
    widthInTiles: number = 0;
    heightInTiles: number = 0;

    private getContextFromStaticFixDef(dir: string): CanvasRenderingContext2D {
        const imageFile = path.join(dir, this.staticFixDef.file);
        const fileData = fs.readFileSync(imageFile);

        const img = new Image();
        // @ts-ignore still figuring out the node-canvas types
        img.src = fileData;

        const canvas = new Canvas();
        canvas.width = img.width;
        canvas.height = img.height;

        const context = canvas.getContext("2d") as CanvasRenderingContext2D;
        context.drawImage(img, 0, 0);

        return context;
    }

    private buildSData(): FixTileBinaryData {
        if (!this.tiles) {
            throw new Error(
                "StaticFixImage: buildSData called before tiles established"
            );
        }

        const sData = this.tiles.reduce(
            (buildingSData, tile) => {
                if (tile.sData) {
                    return buildingSData.concat(tile.sData);
                } else {
                    throw new Error("StaticFixImage: tile has no s data");
                }
            },
            [] as number[]
        );

        return {
            staticFixDef: this.staticFixDef,
            sData
        };
    }

    private buildHeaderData(fixIndex: number): string {
        const lowerName = this.staticFixDef.name;
        const upperName = lowerName.toUpperCase();

        return `
// start STATIC_FIX_${upperName}
#define STATIC_FIX_${upperName} ${fixIndex}

const struct StaticFix ${lowerName}_staticFix;

// end STATIC_FIX_{upperName}
`;
    }

    private getTileStructArrays(tileMap: Map<FixTile, number>): string {
        if (!this.tiles) {
            throw new Error(
                "StaticFixImage: getTileStructArrays called before tiles established"
            );
        }
        const tileEntries = this.tiles.map(tile => {
            return `{ .index = ${tileMap.get(tile)}, .palette = ${
                tile.paletteIndex
            } }`;
        });

        return `const struct FixTile fix_tiles_${this.staticFixDef.name}[${
            this.tiles.length
        }] = {
${tileEntries.join(",\n    ")}
};`;
    }

    private buildSourceData(
        fixIndex: number,
        tileMap: Map<FixTile, number>
    ): string {
        if (!this.tiles) {
            throw new Error(
                "StaticFixImage: buildSourceData called before tiles established"
            );
        }

        const lowerName = this.staticFixDef.name;
        const upperName = lowerName.toUpperCase();

        return `
${this.getTileStructArrays(tileMap)}
const struct StaticFix ${lowerName}_staticFix = {
    .type = STATIC_FIX_${upperName},
    .tiles = fix_tiles_${lowerName},
    .tileCount = ${this.tiles.length},
    .width = ${this.widthInTiles},
    .height = ${this.heightInTiles}
    
};`;
    }

    constructor(private staticFixDef: StaticFixDef) {}

    getTiles(dir: string): FixTile[] {
        if (!this.tiles) {
            const context = this.getContextFromStaticFixDef(dir);
            this.widthInTiles = context.canvas.width / 8;
            this.heightInTiles = context.canvas.height / 8;

            this.tiles = [];

            for (let ty = 0; ty < context.canvas.height; ty += 8) {
                for (let tx = 0; tx < context.canvas.width; tx += 8) {
                    const imageData = context.getImageData(tx, ty, 8, 8);

                    this.tiles.push(new FixTile(imageData.data));
                }
            }
        }

        return this.tiles;
    }

    buildData(
        indexedPaletteDataMap: Map<Raw24BitPalette, PaletteColorData>,
        tileMap: Map<FixTile, number>,
        tileOffset: number,
        fixIndex: number
    ) {
        if (!this.tiles) {
            throw new Error(
                "StaticFixImage: buildData called before tiles established"
            );
        }

        this.tiles.forEach(tile => {
            const indexedPaletteData = indexedPaletteDataMap.get(
                tile.rgbPalette
            );

            if (!indexedPaletteData) {
                throw new Error(
                    "StaticFixImage#buildData: failed to get indexedPaletteData for a tile"
                );
            }

            tile.index(indexedPaletteData);
            tile.createSData();
        });

        const sData = this.buildSData();
        const headerData = this.buildHeaderData(fixIndex);
        const sourceData = this.buildSourceData(fixIndex, tileMap);

        return {
            sData,
            headerData,
            sourceData
        };
    }
}

export { FixTile, StaticFixImage };
