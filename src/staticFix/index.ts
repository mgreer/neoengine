import {
    StaticFixDef,
    FixTileBinaryData,
    SROMFileBinaryData
} from "./interfaces";
import { StaticFixImage } from "./staticFixImage";
import { FixTile } from "../fixTile";
import { Raw24BitPalette, PaletteColorData } from "../palettes/interfaces";
import { padTo } from "../binaryUtils/pad";

export type StaticFixProcessResult = {
    sData: SROMFileBinaryData;
    headerFileData: string;
    sourceFileData: string;
};

// SROM must be at least 128kb
const FULL_SROM_SIZE = 128 * 1024;

class StaticFixes {
    staticFixImages: StaticFixImage[];
    private _tiles: FixTile[] | null = null;

    constructor(staticFixDefs: StaticFixDef[], private inputJsonDir: string) {
        this.staticFixImages = staticFixDefs.map(sf => new StaticFixImage(sf));
    }

    private buildFinalSData(tileData: FixTileBinaryData[]): SROMFileBinaryData {
        const sData = tileData.reduce(
            (buildingSROMData, tileData) => {
                buildingSROMData.sData = buildingSROMData.sData.concat(
                    tileData.sData
                );
                return buildingSROMData;
            },
            { index: 1, sData: [] as number[] }
        );

        sData.sData = padTo(sData.sData, FULL_SROM_SIZE);

        return sData;
    }

    private buildFinalHeaderFileData(headerData: string[]): string {
        const headerFileSrc = `#include "staticFix.h"
${headerData.join("\n\n")}`;

        return headerFileSrc;
    }

    private buildFinalSourceFileData(sourceData: string[]): string {
        const sourceFileSrc = `#include "staticFixDefs.h"
${sourceData.join("\n\n")}`;

        return sourceFileSrc;
    }

    getTiles() {
        if (!this._tiles) {
            this._tiles = this.staticFixImages.reduce(
                (buildingTiles, sf) => {
                    return buildingTiles.concat(sf.getTiles(this.inputJsonDir));
                },
                [] as FixTile[]
            );
        }

        return this._tiles;
    }

    buildData(
        indexedPaletteDataMap: Map<Raw24BitPalette, PaletteColorData>,
        tileOffset: number
    ): StaticFixProcessResult {
        if (!this._tiles) {
            throw new Error("StaticFixes#buildData: called before getTiles");
        }

        const tileMap: Map<FixTile, number> = new Map<FixTile, number>();
        this._tiles.forEach((tile, i) => tileMap.set(tile, i));

        const fixResults = this.staticFixImages.map((sf, i) => {
            const fixResult = sf.buildData(
                indexedPaletteDataMap,
                tileMap,
                tileOffset,
                i
            );

            tileOffset += fixResult.sData.sData.length / 32;

            return fixResult;
        });

        const headerData = fixResults.map(fr => fr.headerData);

        const sourceData = fixResults.map(fr => fr.sourceData);

        return {
            sData: this.buildFinalSData(fixResults.map(fr => fr.sData)),
            headerFileData: this.buildFinalHeaderFileData(headerData),
            sourceFileData: this.buildFinalSourceFileData(sourceData)
        };
    }
}

export { StaticFixes };
