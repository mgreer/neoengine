import {
    Raw24BitColor,
    Raw24BitPalette,
    PaletteColorData
} from "./palettes/interfaces";
import { raw24BitColorToString } from "./palettes/colorUtils";
import { extractRgbPalette } from "./palettes";

class FixTile {
    private _rgbPalette: Raw24BitPalette | null = null;
    paletteIndex: number = 1;
    indexedData: number[] | null = null;
    sData: number[] | null = null;

    private convertToIndexed(
        rawImageData: Uint8ClampedArray,
        indexedPaletteData: PaletteColorData
    ) {
        const indexed: number[] = [];

        for (let i = 0; i < rawImageData.length; i += 4) {
            const currentColor: Raw24BitColor = Array.from(
                rawImageData.slice(i, i + 4)
            ) as Raw24BitColor;

            const index = indexedPaletteData.raw24ColorToIndex.get(
                raw24BitColorToString(currentColor)
            );

            if (index === undefined) {
                throw new Error(
                    "Fix Tile: Convert a tile corner to indexed, failed to find a color"
                );
            }

            indexed.push(index);
        }

        return indexed;
    }

    /**
     * This is the real meat of fix, convert into the actual S ROM
     * data format here
     *
     * The format is explained here: https://wiki.neogeodev.org/index.php?title=Fix_graphics_format
     */
    private getBytesForIndexedData(indexedData: number[]): number[] {
        const sRomData: number[] = [];

        // first 8 bytes is column A, x=4 and x=5
        for (let y = 0; y < 8; ++y) {
            const leftIndexedValue = indexedData[y * 8 + 4];
            const rightIndexedValue = indexedData[y * 8 + 5];

            // the packed byte has the left pixel (image-wise) in bits 0-3
            // and the right pixel (image-wise) in bits 4-7
            const packedByte =
                (leftIndexedValue & 0xf) | ((rightIndexedValue & 0xf) << 4);
            sRomData[y] = packedByte;
        }

        // second 8 bytes is column B, x=6 and x=7
        for (let y = 0; y < 8; ++y) {
            const leftIndexedValue = indexedData[y * 8 + 6];
            const rightIndexedValue = indexedData[y * 8 + 7];

            // the packed byte has the left pixel (image-wise) in bits 0-3
            // and the right pixel (image-wise) in bits 4-7
            const packedByte =
                (leftIndexedValue & 0xf) | ((rightIndexedValue & 0xf) << 4);
            sRomData[y + 8] = packedByte;
        }

        // third 8 bytes is column C, x=0 and x=1
        for (let y = 0; y < 8; ++y) {
            const leftIndexedValue = indexedData[y * 8 + 0];
            const rightIndexedValue = indexedData[y * 8 + 1];

            // the packed byte has the left pixel (image-wise) in bits 0-3
            // and the right pixel (image-wise) in bits 4-7
            const packedByte =
                (leftIndexedValue & 0xf) | ((rightIndexedValue & 0xf) << 4);
            sRomData[y + 16] = packedByte;
        }

        // final 8 bytes is column D, x=2 and x=3
        for (let y = 0; y < 8; ++y) {
            const leftIndexedValue = indexedData[y * 8 + 2];
            const rightIndexedValue = indexedData[y * 8 + 3];

            // the packed byte has the left pixel (image-wise) in bits 0-3
            // and the right pixel (image-wise) in bits 4-7
            const packedByte =
                (leftIndexedValue & 0xf) | ((rightIndexedValue & 0xf) << 4);
            sRomData[y + 24] = packedByte;
        }

        return sRomData;
    }

    constructor(private rgbData: Uint8ClampedArray) {}

    get rgbPalette(): Raw24BitPalette {
        if (!this._rgbPalette) {
            this._rgbPalette = extractRgbPalette(this.rgbData);
        }

        return this._rgbPalette;
    }

    index(indexedPaletteData: PaletteColorData) {
        this.paletteIndex = indexedPaletteData.paletteIndex;

        this.indexedData = this.convertToIndexed(
            this.rgbData,
            indexedPaletteData
        );
    }

    createSData() {
        if (!this.indexedData) {
            throw new Error(
                "Fix Tile#createSData: called before indexedData created"
            );
        }

        this.sData = this.getBytesForIndexedData(this.indexedData);
    }
}

export { FixTile };
