// declare class Canvas extends HTMLCanvasElement {
//     constructor(width: number, height: number);
//     getContext(
//         contextId: "2d",
//         contextAttributes?: Canvas2DContextAttributes
//     ): NodeCanvasCanvasRenderingContext2D;
// }

interface NodeCanvasCanvasRenderingContext2D extends CanvasRenderingContext2D {
    antialias: "default" | "gray" | "none" | "subpixel";
}

declare module "canvas" {
    export function createImageData(
        data: Uint8ClampedArray | Uint16Array,
        width: number,
        height?: number
    ): ImageData;
    export function createCanvas(width: number, height: number): NodeCanvas;
    export class Canvas extends HTMLCanvasElement {
        public createPNGStream(): any {}
    }
    export class Image extends HTMLImageElement {}
}
