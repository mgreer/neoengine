import fs from "fs";
import { Canvas, Image } from "canvas";

export function copyImageData(input: Uint8ClampedArray, dest: ImageData) {
    input.forEach((val, i) => (dest.data[i] = val));
}

export function createInMemoryContext(
    width: number,
    height: number
): CanvasRenderingContext2D {
    const canvas = new Canvas();
    canvas.width = width;
    canvas.height = height;

    return canvas.getContext("2d") as CanvasRenderingContext2D;
}

export function getContextFromImageFile(
    path: string
): CanvasRenderingContext2D {
    const fileData = fs.readFileSync(path);

    const img = new Image();
    // @ts-ignore still figuring out the node-canvas types
    img.src = fileData;

    const canvas = new Canvas();
    canvas.width = img.width;
    canvas.height = img.height;

    const context = canvas.getContext("2d") as CanvasRenderingContext2D;
    context.drawImage(img, 0, 0);

    return context;
}
